﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using ADS.Core.Model;
using ADS.Core.Repositorio;
using ADS.Core.Unit;
using ADS.Core.Exceptions;

namespace ADS.Core.Repository
{
    public class FabricanteRepository : IFabricanteRepository
    {
        private ADSContext _context;

        public FabricanteRepository(ADSContext context)
        {
            _context = context;
        }

        public void Alterar(Fabricante marca)
        {
            _context.Entry(marca).State = System.Data.Entity.EntityState.Modified;
        }

        public Fabricante Buscar(int id)
        {
            return _context.Fabricantes.Find(id);
        }

        public ICollection<Fabricante> BuscarPor(Expression<Func<Fabricante, bool>> filtro)
        {
            return _context.Fabricantes.Where(filtro).ToList();
        }

        public void Cadastrar(Fabricante marca)
        {
            _context.Fabricantes.Add(marca);
        }

        public ICollection<Fabricante> Listar()
        {
            return _context.Fabricantes.OrderBy(f => f.Nome).ToList();
        }

        public ICollection<Fabricante> ListarPaginado(Pager pager)
        {
            return _context.Fabricantes.Where(f => f.Removido == false)
                .OrderBy(f => f.Nome)
                .Skip((pager.CurrentPage - 1) * pager.PageSize)
                .Take(pager.PageSize)
                .ToList();
        }

        public int QuantidadeFabricante()
        {
            return _context.Fabricantes.Where(f=>f.Removido==false).Count();
        }

        public void Remover(int id)
        {
            var marca = _context.Fabricantes.Find(id);
            if (_context.Modelos.Where(m => m.FabricanteId == marca.FabricanteId).Count() > 0)
            {
                throw new ModelosAssociadosException("Existem modelos associados a esse fabricante");
            }
            else
            {
                _context.Fabricantes.Remove(marca);
            }
        }
    }
}