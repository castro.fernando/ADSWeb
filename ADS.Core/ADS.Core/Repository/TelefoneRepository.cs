﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using ADS.Core.Model;
using ADS.Core.Repositorio;

namespace ADS.Core.Repository
{
    public class TelefoneRepository : ITelefoneRepository
    {
        private ADSContext _context;

        public TelefoneRepository(ADSContext context)
        {
            _context = context;
        }

        public void Alterar(Telefone telefone)
        {
            _context.Entry(telefone).State = System.Data.Entity.EntityState.Modified;
        }

        public Telefone Buscar(int id)
        {
            return _context.Telefones.Find(id);
        }

        public ICollection<Telefone> BuscarPor(Expression<Func<Telefone, bool>> filtro)
        {
            return _context.Telefones.Where(filtro).ToList();
        }

        public void Cadastrar(Telefone telefone)
        {
            _context.Telefones.Add(telefone);
        }

        public ICollection<Telefone> Listar()
        {
            return _context.Telefones.ToList();
        }

        public void Remover(int id)
        {
            var tel = _context.Telefones.Find(id);
            _context.Telefones.Remove(tel);
        }
    }
}