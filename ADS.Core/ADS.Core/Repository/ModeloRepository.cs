﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using ADS.Core.Model;
using ADS.Core.Repositorio;
using ADS.Core.Exceptions;

namespace ADS.Core.Repository
{
    public class ModeloRepository : IModeloRepository
    {
        private ADSContext _context;

        public ModeloRepository(ADSContext context)
        {
            _context = context;
        }

        public void Alterar(Modelo modelo)
        {
            _context.Entry(modelo).State = System.Data.Entity.EntityState.Modified;
        }

        public Modelo Buscar(int id)
        {
            return _context.Modelos.Find(id);
        }

        public ICollection<Modelo> BuscarPor(Expression<Func<Modelo, bool>> filtro)
        {
            return _context.Modelos.Include("Fabricante").Where(filtro).OrderBy(c => c.Fabricante.Nome).ThenBy(c => c.Nome).ToList();
        }

        public void Cadastrar(Modelo modelo)
        {
            _context.Modelos.Add(modelo);
        }

        public ICollection<Modelo> Listar()
        {
            return _context.Modelos.Include("Fabricante").OrderBy(c=> c.Fabricante.Nome).ThenBy(c=>c.Nome).ToList();
        }

        public ICollection<Modelo> ListarPaginado(Pager pager)
        {
            return _context.Modelos.Include("Fabricante")
                .Where(m => m.Removido == false)
                .OrderBy(m => m.Fabricante.Nome)
                .ThenBy(m=>m.Nome)
                .Skip((pager.CurrentPage - 1) * pager.PageSize)
                .Take(pager.PageSize)
                .ToList();
        }

        public ICollection<Modelo> ListarPorFabricante(int id)
        {
            return _context.Modelos.Include("Fabricante").Where(m=>m.FabricanteId == id).ToList();
        }

        public int QuantidadeModelo()
        {
            return _context.Modelos.Where(m => m.Removido == false).Count();
        }

        public void Remover(int id)
        {
            var modelo = _context.Modelos.Find(id);
            if(_context.Veiculos.Where(m=>m.ModeloId == modelo.ModeloId).Count() > 0)
            {
                throw new ModelosAssociadosException("Existem veiculos associados a esse modelo");
            }
            else
            {
                _context.Modelos.Remove(modelo);
            }
            
        }
    }
}