﻿using ADS.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ADS.Core.Repository
{
    public interface IVeiculoRepository
    {
        void Cadastrar(Veiculo veiculo);
        void Alterar(Veiculo veiculo);
        void Remover(int id);
        Veiculo Buscar(int id);
        ICollection<Veiculo> Listar();
        ICollection<Veiculo> BuscarPor(Expression<Func<Veiculo, bool>> filtro);
    }
}
