﻿using ADS.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ADS.Core.Repository
{
    public interface IAdvogadoRepository
    {
        void Cadastrar(Advogado advogado);
        void Alterar(Advogado advogado);
        void Remover(int id);
        Advogado Buscar(int id);
        ICollection<Advogado> Listar();
        int QuantidadeDeAdvogados();
        ICollection<Advogado> ListarPaginado(Pager pager);
        ICollection<Advogado> BuscarPor(Expression<Func<Advogado, bool>> filtro);
    }
}
