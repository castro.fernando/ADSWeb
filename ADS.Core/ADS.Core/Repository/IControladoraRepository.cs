﻿using ADS.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ADS.Core.Repository
{
    public interface IControladoraRepository
    {
        void Cadastrar(Controladora controladora);
        void Alterar(Controladora controladora);
        void Remover(int id);
        Controladora Buscar(int id);
        int QuantidadeControladora();
        ICollection<Controladora> ListarPaginado(Pager pager);
        ICollection<Controladora> Listar();
        ICollection<Controladora> BuscarPor(Expression<Func<Controladora, bool>> filtro);
    }
}
