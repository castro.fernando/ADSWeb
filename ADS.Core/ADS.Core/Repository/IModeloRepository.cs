﻿using ADS.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ADS.Core.Repository
{
    public interface IModeloRepository
    {
        void Cadastrar(Modelo modelo);
        void Alterar(Modelo modelo);
        void Remover(int id);
        Modelo Buscar(int id);
        ICollection<Modelo> Listar();
        ICollection<Modelo> BuscarPor(Expression<Func<Modelo, bool>> filtro);
        ICollection<Modelo> ListarPorFabricante(int id);
        ICollection<Modelo> ListarPaginado(Pager pager);
        int QuantidadeModelo();
    }
}
