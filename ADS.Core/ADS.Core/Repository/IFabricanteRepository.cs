﻿using ADS.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ADS.Core.Repository
{
    public interface IFabricanteRepository
    {
        void Cadastrar(Fabricante fabricante);
        void Alterar(Fabricante fabricante);
        void Remover(int id);
        Fabricante Buscar(int id);
        ICollection<Fabricante> Listar();
        ICollection<Fabricante> ListarPaginado(Pager pager);
        int QuantidadeFabricante();
        ICollection<Fabricante> BuscarPor(Expression<Func<Fabricante, bool>> filtro);
    }
}
