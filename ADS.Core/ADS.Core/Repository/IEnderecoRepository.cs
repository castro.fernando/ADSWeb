﻿using ADS.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ADS.Core.Repository
{
    public interface IEnderecoRepository
    {
        void Cadastrar(Endereco endereco);
        void Alterar(Endereco endereco);
        void Remover(int id);
        Endereco Buscar(int id);
        ICollection<Endereco> Listar();
        ICollection<Endereco> BuscarPor(Expression<Func<Endereco, bool>> filtro);
    }
}
