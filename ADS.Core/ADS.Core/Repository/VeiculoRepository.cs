﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using ADS.Core.Model;
using ADS.Core.Repositorio;

namespace ADS.Core.Repository
{
    public class VeiculoRepository : IVeiculoRepository
    {
        private ADSContext _context;

        public VeiculoRepository(ADSContext context)
        {
            _context = context;
        }
        public void Alterar(Veiculo veiculo)
        {
            _context.Entry(veiculo).State = System.Data.Entity.EntityState.Modified;
        }

        public Veiculo Buscar(int id)
        {
            return _context.Veiculos
                .Include("Modelo")
                .FirstOrDefault();
        }

        public ICollection<Veiculo> BuscarPor(Expression<Func<Veiculo, bool>> filtro)
        {
            return _context.Veiculos
                .Include("Modelo")
                .Where(filtro)
                .ToList();
        }

        public void Cadastrar(Veiculo veiculo)
        {
            _context.Veiculos.Add(veiculo);
        }

        public ICollection<Veiculo> Listar()
        {
            return _context.Veiculos.ToList();
        }

        public void Remover(int id)
        {
            var veiculo = _context.Veiculos.Find(id);
            _context.Veiculos.Remove(veiculo);
        }
    }
}