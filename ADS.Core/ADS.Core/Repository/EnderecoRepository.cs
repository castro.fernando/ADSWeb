﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using ADS.Core.Model;
using ADS.Core.Repositorio;

namespace ADS.Core.Repository
{
    public class EnderecoRepository : IEnderecoRepository
    {
        private ADSContext _context;

        public EnderecoRepository(ADSContext context)
        {
            _context = context;
        }

        public void Alterar(Endereco endereco)
        {
            _context.Entry(endereco).State = System.Data.Entity.EntityState.Modified;
        }

        public Endereco Buscar(int id)
        {
            return _context.Enderecos.Find(id);
        }

        public ICollection<Endereco> BuscarPor(Expression<Func<Endereco, bool>> filtro)
        {
           return _context.Enderecos.Where(filtro).ToList();
        }

        public void Cadastrar(Endereco endereco)
        {
            _context.Enderecos.Add(endereco);
        }

        public ICollection<Endereco> Listar()
        {
            return _context.Enderecos.ToList();
        }

        public void Remover(int id)
        {
            var end = _context.Enderecos.Find(id);
            _context.Enderecos.Remove(end);
        }
    }
}