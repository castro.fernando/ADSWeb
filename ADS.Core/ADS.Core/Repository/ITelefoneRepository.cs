﻿using ADS.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ADS.Core.Repository
{
    public interface ITelefoneRepository
    {
        void Cadastrar(Telefone telefone);
        void Alterar(Telefone telefone);
        void Remover(int id);
        Telefone Buscar(int id);
        ICollection<Telefone> Listar();
        ICollection<Telefone> BuscarPor(Expression<Func<Telefone, bool>> filtro);
    }
}
