﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using ADS.Core.Model;
using ADS.Core.Repositorio;

namespace ADS.Core.Repository
{
    public class ControladoraRepository : IControladoraRepository
    {
        private ADSContext _context;

        public ControladoraRepository(ADSContext context)
        {
            _context = context;
        }

        public void Alterar(Controladora controladora)
        {
            _context.Entry(controladora).State = System.Data.Entity.EntityState.Modified;
        }

        public Controladora Buscar(int id)
        {
            return _context.Controladoras.FirstOrDefault(c => c.ControladoraId == id);
        }

        public ICollection<Controladora> BuscarPor(Expression<Func<Controladora, bool>> filtro)
        {
            return _context.Controladoras.OrderBy(c=>c.Nome).Where(filtro).ToList();
        }

        public void Cadastrar(Controladora controladora)
        {
            _context.Controladoras.Add(controladora);
        }

        public ICollection<Controladora> Listar()
        {
            return _context.Controladoras.Where(c => c.Removido == false).OrderBy(c => c.Nome).ToList();
        }

        public int QuantidadeControladora()
        {
            return _context.Controladoras.Where(c=>c.Removido == false).Count();
        }

        public void Remover(int id)
        {
            var controladora =_context.Controladoras.FirstOrDefault(c => c.ControladoraId == id);
            controladora.Removido = true;
            controladora.RemovidoEm = Convert.ToDateTime(System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            _context.Entry(controladora).State = System.Data.Entity.EntityState.Modified;
        }

        public ICollection<Controladora> ListarPaginado(Pager pager)
        {
            return _context.Controladoras.Where(f => f.Removido == false)
                .OrderBy(f => f.Nome)
                .Skip((pager.CurrentPage - 1) * pager.PageSize)
                .Take(pager.PageSize)
                .ToList();
        }
    }
}