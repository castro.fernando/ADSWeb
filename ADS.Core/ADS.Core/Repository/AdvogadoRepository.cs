﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using ADS.Core.Model;
using ADS.Core.Repositorio;

namespace ADS.Core.Repository
{
    public class AdvogadoRepository : IAdvogadoRepository
    {
        private ADSContext _context;

        public AdvogadoRepository(ADSContext context)
        {
            _context = context;
        }

        public void Alterar(Advogado advogado)
        {
            foreach (var v in advogado.Veiculo)
            {
                _context.Entry(v.Modelo).State = System.Data.Entity.EntityState.Unchanged;
            }

            _context.Entry(advogado).State = System.Data.Entity.EntityState.Modified;
        }

        public Advogado Buscar(int id)
        {
            var adv = _context.Advogados
                    .Include("Endereco")
                    .Include("Veiculo")
                    .Include("Telefone")
                .Where(a => a.AdvogadoId == id);
            return adv.FirstOrDefault();
            
        }

        public ICollection<Advogado> BuscarPor(Expression<Func<Advogado, bool>> filtro)
        {
            return _context.Advogados
                 .Include("Endereco")
                 .Include("Veiculo")
                 .Include("Telefone")
                .Where(filtro).ToList();
        }

        public void Cadastrar(Advogado advogado)
        {
            foreach(var v in advogado.Veiculo)
            {
               _context.Entry(v.Modelo).State = System.Data.Entity.EntityState.Unchanged;
            }
            _context.Advogados.Add(advogado);
        }

        public ICollection<Advogado> Listar()
        {
            return _context.Advogados.ToList();
        }

        public ICollection<Advogado> ListarPaginado(Pager pager)
        {
            return _context.Advogados.Where(a => a.Removido == false)
                .OrderBy(a => a.Nome)
                .Skip((pager.CurrentPage - 1) * pager.PageSize)
                .Take(pager.PageSize)
                .ToList();
        }

        public int QuantidadeDeAdvogados()
        {
            return _context.Advogados.Where(a => a.Removido == false) .Count();
        }

        public void Remover(int id)
        {
            _context.Advogados.Remove(_context.Advogados.Find(id));
        }

    }
}
