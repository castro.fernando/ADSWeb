﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace ADS.Core.Model
{
    public class Veiculo
    {
        public int VeiculoId{ get; set; }
        public virtual Modelo Modelo { get; set; }
        public int ModeloId { get; set; }
        public string Placa { get; set; }
        [JsonIgnore]
        [ScriptIgnore]
        public virtual Advogado Advogado { get; set; }
        public int AdvogadoId { get; set; }

        public bool Removido { get; set; }
        public DateTime? RemovidoEm { get; set; }
        public DateTime? AtualizadoEm { get; set; }
        public DateTime? CriadoEm { get; set; }
    }
}