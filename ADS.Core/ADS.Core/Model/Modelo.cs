﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ADS.Core.Model
{
    public class Modelo
    {
        public int ModeloId { get; set; }

        [Display(Name = "Fabricante")]
        public virtual Fabricante Fabricante { get; set; }

        [Required(ErrorMessage = "A marca do carro é obrigatória")]
        public int FabricanteId { get; set; }

        [Display(Name = "Modelo")]
        [Required (ErrorMessage = "O nome é obrigatório")]
        [StringLength(50, MinimumLength=2, ErrorMessage = "O nome deve possuir entre 2 ~ 50 caractéres")]
        public string Nome { get; set; }

        public bool Removido { get; set; }
        public DateTime? RemovidoEm { get; set; }
        public DateTime? AtualizadoEm { get; set; }
        public DateTime? CriadoEm { get; set; }
    }
}