﻿using System;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace ADS.Core.Model
{
    public class Fabricante
    {
        public int FabricanteId { get; set; }

        [Required]
        [StringLength(100,MinimumLength = 5, ErrorMessage = "O campo nome deve ter de 5 a 100 caractéres")]
        public string Nome { get; set; }

        public virtual ICollection Modelo { get; set; }

        public bool Removido { get; set; }
        public DateTime? RemovidoEm { get; set; }
        public DateTime? AtualizadoEm { get; set; }
        public DateTime? CriadoEm { get; set; }
    }
}