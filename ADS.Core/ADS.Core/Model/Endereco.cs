﻿using Newtonsoft.Json;
using System;
using System.Web.Script.Serialization;

namespace ADS.Core.Model
{
    public class Endereco
    {
        public int EnderecoId { get; set; }
        public string CEP { get; set; }
        public string UF { get; set; }
        public string Cidade { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Logradouro { get; set; }
        public int Numero { get; set; }

        [JsonIgnore]
        [ScriptIgnore]
        public virtual Advogado Advogado { get; set; }
        public int AdvogadoId { get; set; }
        public bool Removido { get; set; }
        public DateTime? RemovidoEm { get; set; }
        public DateTime? AtualizadoEm { get; set; }
        public DateTime? CriadoEm { get; set; }
    }
}