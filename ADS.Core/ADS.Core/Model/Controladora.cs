﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ADS.Core.Model
{
    public class Controladora
    {
        public int ControladoraId { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public string Ip { get; set; }
        [Required]
        public string Mascara { get; set; }
        [Required]
        public string Gateway { get; set; }
        [Required]
        [Display(Name = "Ip de Destino")]
        public string IpDestino { get; set; }
        [Required]
        [Display(Name = "Porta de Destino")]
        public int PortaDestino { get; set; }
        [Display(Name = "Porta de Origem")]
        public int PortaOrigem { get; set; }
        [Display(Name = "Endereço Mac")]
        public string MacAddress { get; set; }
        public bool Ativo { get; set; }
        public bool Removido { get; set; }
        public DateTime? RemovidoEm { get; set; }
        public DateTime? AtualizadoEm { get; set; }
        public DateTime? CriadoEm { get; set; }
    }
}