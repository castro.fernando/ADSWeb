﻿using Newtonsoft.Json;
using System;
using System.Web.Script.Serialization;

namespace ADS.Core.Model
{
    public class Telefone
    {
        public int TelefoneId { get; set; }
        public int DDD { get; set; }
        public string Numero { get; set; }
        [JsonIgnore]
        [ScriptIgnore]
        public virtual Advogado Advogado { get; set; }
        public int AdvogadoId { get; set; }
        public bool Removido { get; set; }
        public DateTime? RemovidoEm { get; set; }
        public DateTime? AtualizadoEm { get; set; }
        public DateTime? CriadoEm { get; set; }
    }
}