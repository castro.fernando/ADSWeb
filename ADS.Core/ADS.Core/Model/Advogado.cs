﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Newtonsoft.Json;

namespace ADS.Core.Model
{
    public class Advogado
    {
        public int AdvogadoId { get; set; }

        [Required(ErrorMessage = "O nome é obrigatório")]
        [Display(Name = "Nome*")]
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string RG { get; set; }

        [Required(ErrorMessage = "O número da carteira é obrigatório")]
        [Display(Name = "Nr. Carteira*")]
        public Int64 NumeroCarteira { get; set; }

        [Required(ErrorMessage = "O número da inscrição é obrigatório" )]
        [Display(Name = "Nr. Inscrição*")]
        public Int64 NumeroInscricao { get; set; }

        public string Seccional { get; set; }
        public string Subsecao { get; set; }
        public byte[] Foto { get; set; }
        public DateTime? ExpedidoEm { get; set; }
        public string Filiacao { get; set; }
        public virtual IList<Endereco> Endereco { get; set; }
        public virtual IList<Telefone> Telefone { get; set; }
        public virtual IList<Veiculo> Veiculo { get; set; }
        public bool Ativo { get; set; }
        public bool Removido { get; set; }
        public DateTime? RemovidoEm { get; set; }
        public DateTime? AtualizadoEm { get; set; }
        public DateTime? CriadoEm { get; set; }
    }
}