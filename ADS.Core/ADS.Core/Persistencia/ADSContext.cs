﻿using ADS.Core.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Reflection.Emit;
using System.Web;

namespace ADS.Core.Repositorio
{
    public class ADSContext : DbContext
    {
        public DbSet<Endereco> Enderecos { get; set; }
        public DbSet<Fabricante> Fabricantes { get; set; }
        public DbSet<Modelo> Modelos { get; set; }
        public DbSet<Advogado> Advogados { get; set; }
        public DbSet<Telefone> Telefones { get; set; }
        public DbSet<Veiculo> Veiculos { get; set; }
        public DbSet<Controladora> Controladoras { get; set; }


        public ADSContext():base("name=ADSContext")
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }

}