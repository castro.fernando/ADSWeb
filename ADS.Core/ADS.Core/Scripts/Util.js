﻿$(document).ready(function () {
    $.get("../Advogado/GetVeiculos",
        function (result) {
            $('#linhasTabVeiculo').html(result);
        });
    $.get("../Advogado/GetEnderecos",
        function (result) {
            $('#linhasTabEndereco').html(result);
        });
    $.get("../Advogado/GetTelefones",
        function (result) {
            $('#linhasTabTelefone').html(result);
        });
});


//Adicionar veiculo -Mudança de dropbox fabricante / modelo
$('#SelectFabricante').on('change', function (e) {
    var optionSelected = $("option:selected", this);
    var id = this.value;
    if (id !== "") {
        $.post("../Advogado/GetModelosJson", { "id": id },
            function (result) {
                $('#SelectModelo').empty();
                $('#SelectModelo').append('<option value="">Selecione</option>');
                for (var i = 0; i < result.Modelos.length; i++) {
                    $('#SelectModelo').append('<option value="' + result.Modelos[i].ModeloId + '">' + result.Modelos[i].Nome + '</option>');    
                }
            });
    } else {
        $('#SelectModelo').empty();
        $('#SelectModelo').append('<option value="">Selecione</option>');
    }
});

function limparModalVeiculo() {
    $('#VeiculoId').val('');
    $('#Placa').val('');
    $('#SelectFabricante').val('');
    $('#SelectModelo').val('');
}

$('#salvarVeiculo').on('click', function (e) {
    if ($('#SelectFabricante').val() !== '' && $('#SelectModelo').val() !== '')
    {
        var objVeiculo = {
            VeiculoId: $('#VeiculoId').val(),
            Fabricante: $('#SelectFabricante').val(),
            ModeloId: $('#SelectModelo').val(),
            Placa: $('#Placa').val()
        };
        var veiculo = JSON.stringify(objVeiculo);
        $.ajax({
            method: 'POST',
            url: "../Advogado/AdicionarVeiculo",
            data: veiculo,
            dataType: "html",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                $('#linhasTabVeiculo').html(result);
                $('#modalAdicionarVeiculo').modal('hide');
                limparModalVeiculo();
            },
            error: function () {
                alert('Falha ao inserir registro na lista.Consulte o admin do sistema.');
            }
        });
    } else {
        alert('Fabricante e Modelo são requeridos!');
    }
    
});

$('#fecharModalVeiculo').on('click', function (e) {
    limparModalVeiculo();
    $('#modalAdicionarVeiculo').modal('hide');
});

function editarVeiculo(linha) {
    $.ajax({
        method: 'GET',
        url: "../Advogado/BuscarVeiculo/" + linha,
        success: function (result) {
            $('#VeiculoId').val(linha);
            $('#SelectFabricante').val(result.Veiculo.Modelo.FabricanteId);
            $('#SelectFabricante').trigger('change');
            $('#SelectModelo').val(result.Veiculo.ModeloId);
            $('#Placa').val(result.Veiculo.Placa);
            $('#modalAdicionarVeiculo').modal('show');
        }
    });
}

function removerVeiculo(linha) {
    $.ajax({
        method: 'POST',
        url: "../Advogado/RemoverVeiculo/" + linha,
        success: function (result) {
            $('#linhasTabVeiculo').html(result);
        }
    });
}

//JS de Imagem
$(document).on('change', ':file', function () {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});

$(document).ready(function () {
    $(':file').on('fileselect', function (event, numFiles, label) {
        $('#arquivoSelecionado').text(label);
        console.log(numFiles);
        console.log(label);
        readURL(this);
    });
});


function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#fotoAdvogado').attr('src', e.target.result);
            var strImage = e.target.result.replace(/^data:image\/[a-z]+;base64,/, "");
            $('#Foto').val(strImage);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

//Integraçao com viaCEP - internet
$('#buscarEndereco').on('click', function (e) {
    var cep = $('#cep').val();

    $.ajax({
        method: 'GET',
        url: "http://viacep.com.br/ws/" + cep + "/json/",
        beforeSend: function () {
            //alert('teste de envio');
            $('#buscando').text('Aguarde, buscando dados...');
        },
        success: function (result) {
            $('#UF').val(result.uf);
            $('#Cidade').val(result.localidade);
            $('#Bairro').val(result.bairro);
            $('#Logradouro').val(result.logradouro);
            $('#buscando').text('');
        },
        error: function () {
            $('#buscando').text('Dados inválidos ou sem conexão com a internet.');
        }
    });
});

function limparModalEndereco() {
    $('#EnderecoId').val('');
    $('#cep').val('');
    $('#UF').val('');
    $('#Cidade').val('');
    $('#Bairro').val('');
    $('#Logradouro').val('');
    $('#Numero').val('');
    $('#Complemento').val('');
}

$('#fecharModal').on('click', function (e) {
    limparModalEndereco();
    $('#buscando').text('');
    $('#modalAdicionarEndereco').modal('hide');
});


$('#salvarEndereco').on('click', function (e) {
    var objEndereco = {
        EnderecoId: $('#EnderecoId').val(),
        cep: $('#cep').val(),
        UF: $('#UF').val(),
        Cidade: $('#Cidade').val(),
        Bairro: $('#Bairro').val(),
        Logradouro: $('#Logradouro').val(),
        Numero: $('#Numero').val(),
        Complemento: $('#Complemento').val()
    };
    var endereco = JSON.stringify(objEndereco);
    $.ajax({
        method: 'POST',
        url: "../Advogado/AdicionarEndereco",
        data: endereco,
        dataType: "html",
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            $('#linhasTabEndereco').html(result);
            $('#modalAdicionarEndereco').modal('hide');
            limparModalEndereco();
        },
        error: function () {
            alert('Falha ao inserir registro na lista.Consulte o admin do sistema.');
        }
    });
});

function editarEndereco(linha) {
    $.ajax({
        method: 'GET',
        url: "../Advogado/BuscarEndereco/" + linha,
        success: function (result) {
            $('#EnderecoId').val(linha);
            $('#cep').val(result.Endereco.CEP);
            $('#UF').val(result.Endereco.UF);
            $('#Cidade').val(result.Endereco.Cidade);
            $('#Bairro').val(result.Endereco.Bairro);
            $('#Logradouro').val(result.Endereco.Logradouro);
            $('#Numero').val(result.Endereco.Numero);
            $('#Complemento').val(result.Endereco.Complemento);
            $('#modalAdicionarEndereco').modal('show');
            $('#buscando').text('');          
        }
    });
}

function detalhesEndereco(linha) {
    $.ajax({
        method: 'GET',
        url: "../Advogado/DetalhesEndereco/" + linha,
        success: function (result) {
            $('#detalhesEnderecoBody').html(result);
            $('#modalDetalhesEndereco').modal('show');
        }
    });
}

function removerEndereco(linha) {
    $.ajax({
        method: 'POST',
        url: "../Advogado/RemoverEndereco/" + linha,
        success: function (result) {
            $('#linhasTabEndereco').html(result);
        }
    });
}

function limparModalTelefone() {
    $('#TelefoneId').val('');
    $('#nrTelefone').val('');
    $('#DDD').val('');
}

//JS de telefone
$('#fecharModalTelefone').on('click', function (e) {
    limparModalTelefone();
    $('#modalAdicionarTelefone').modal('hide');
});

$('#salvarTelefone').on('click', function (e) {
    var objTelefone = {
        TelefoneId: $('#TelefoneId').val(),
        DDD: $('#DDD').val(),
        Numero: $('#nrTelefone').val()
    };
    var telefone = JSON.stringify(objTelefone);
    $.ajax({
        method: 'POST',
        url: "../Advogado/AdicionarTelefone",
        data: telefone,
        dataType: "html",
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            $('#linhasTabTelefone').html(result);
            $('#modalAdicionarTelefone').modal('hide');
            limparModalTelefone();
        },
        error: function () {
            alert('Falha ao inserir registro na lista.Consulte o admin do sistema.');
        }
    });
});

function editarTelefone(linha) {
    $.ajax({
        method: 'GET',
        url: "../Advogado/BuscarTelefone/" + linha,
        success: function (result) {
            $('#TelefoneId').val(linha);
            $('#DDD').val(result.Telefone.DDD);
            $('#nrTelefone').val(result.Telefone.Numero);          
            $('#modalAdicionarTelefone').modal('show');
        }
    });
}

function removerTelefone(linha) {
    $.ajax({
        method: 'POST',
        url: "../Advogado/RemoverTelefone/" + linha,
        success: function (result) {
            $('#linhasTabTelefone').html(result);
        }
    });
}
