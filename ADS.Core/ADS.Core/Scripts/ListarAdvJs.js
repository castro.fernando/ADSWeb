﻿$(function () {
    $(":checkbox").change(function () {
        $.post("../Advogado/gravarCheckboxSelecionado", { id: this.id, selecionado: this.checked });
    });
});

function removerAdvogado(id) {
    $.post("../Advogado/Remover", { id: id }, function (result) {
        var paginaAtual = $('#paginaAtual').val();
        location.href = "../Advogado/Listar?pagina=" + paginaAtual;
    });
}

function detalhesAdvogado(id) {
    $.get("../Advogado/DetalhesAdvogado/" + id, function (result) {
        $('#detalhesAdvogadoBody').html(result);
    });
    $('#modalDetalhesAdvogado').modal('show');
}