﻿$(document).ready(function () {
});

function detalhesControladora(id) {
    $.ajax({
        method: 'GET',
        url: "../Controladora/Detalhes/" + id,
        success: function (result) {
            $('#detalhesControladoraBody').html(result);
            $('#modalDetalhesControladora').modal('show');
        }
    });
}

function removerControladora(id) {
    $.post("../Controladora/Remover", { id: id }, function (result) {
        var paginaAtual = $('#paginaAtual').val();
        location.href = "../Controladora/Listar?pagina=" + paginaAtual;
    });
}