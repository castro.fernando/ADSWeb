namespace ADS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mudancanomodeldeadvogado : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Advogado", "NumeroCarteira", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Advogado", "NumeroCarteira");
        }
    }
}
