namespace ADS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modificadonomeemveiculo : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Veiculo", "Pessoa_AdvogadoId", "dbo.Advogado");
            DropIndex("dbo.Veiculo", new[] { "Pessoa_AdvogadoId" });
            RenameColumn(table: "dbo.Veiculo", name: "Pessoa_AdvogadoId", newName: "AdvogadoId");
            AlterColumn("dbo.Veiculo", "AdvogadoId", c => c.Int(nullable: false));
            CreateIndex("dbo.Veiculo", "AdvogadoId");
            AddForeignKey("dbo.Veiculo", "AdvogadoId", "dbo.Advogado", "AdvogadoId", cascadeDelete: true);
            DropColumn("dbo.Veiculo", "PessoaId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Veiculo", "PessoaId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Veiculo", "AdvogadoId", "dbo.Advogado");
            DropIndex("dbo.Veiculo", new[] { "AdvogadoId" });
            AlterColumn("dbo.Veiculo", "AdvogadoId", c => c.Int());
            RenameColumn(table: "dbo.Veiculo", name: "AdvogadoId", newName: "Pessoa_AdvogadoId");
            CreateIndex("dbo.Veiculo", "Pessoa_AdvogadoId");
            AddForeignKey("dbo.Veiculo", "Pessoa_AdvogadoId", "dbo.Advogado", "AdvogadoId");
        }
    }
}
