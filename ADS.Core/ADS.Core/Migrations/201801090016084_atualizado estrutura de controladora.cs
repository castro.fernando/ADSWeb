namespace ADS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class atualizadoestruturadecontroladora : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Controladora", "PortaDestino", c => c.Int(nullable: false));
            AddColumn("dbo.Controladora", "PortaOrigem", c => c.Int(nullable: false));
            DropColumn("dbo.Controladora", "Porta");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Controladora", "Porta", c => c.Int(nullable: false));
            DropColumn("dbo.Controladora", "PortaOrigem");
            DropColumn("dbo.Controladora", "PortaDestino");
        }
    }
}
