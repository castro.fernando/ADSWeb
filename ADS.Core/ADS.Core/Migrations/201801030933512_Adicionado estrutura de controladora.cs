namespace ADS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Adicionadoestruturadecontroladora : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Controladora",
                c => new
                    {
                        ControladoraId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        Ip = c.String(),
                        Porta = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ControladoraId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Controladora");
        }
    }
}
