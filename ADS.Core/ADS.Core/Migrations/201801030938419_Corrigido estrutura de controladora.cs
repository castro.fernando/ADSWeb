namespace ADS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Corrigidoestruturadecontroladora : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Controladora", "Mascara", c => c.String(nullable: false));
            AddColumn("dbo.Controladora", "Gateway", c => c.String(nullable: false));
            AddColumn("dbo.Controladora", "IpDestino", c => c.String(nullable: false));
            AddColumn("dbo.Controladora", "MacAddress", c => c.String());
            AddColumn("dbo.Controladora", "Ativo", c => c.Boolean(nullable: false));
            AddColumn("dbo.Controladora", "Removido", c => c.Boolean(nullable: false));
            AddColumn("dbo.Controladora", "RemovidoEm", c => c.DateTime());
            AddColumn("dbo.Controladora", "AtualizadoEm", c => c.DateTime());
            AddColumn("dbo.Controladora", "CriadoEm", c => c.DateTime());
            AlterColumn("dbo.Controladora", "Nome", c => c.String(nullable: false));
            AlterColumn("dbo.Controladora", "Ip", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Controladora", "Ip", c => c.String());
            AlterColumn("dbo.Controladora", "Nome", c => c.String());
            DropColumn("dbo.Controladora", "CriadoEm");
            DropColumn("dbo.Controladora", "AtualizadoEm");
            DropColumn("dbo.Controladora", "RemovidoEm");
            DropColumn("dbo.Controladora", "Removido");
            DropColumn("dbo.Controladora", "Ativo");
            DropColumn("dbo.Controladora", "MacAddress");
            DropColumn("dbo.Controladora", "IpDestino");
            DropColumn("dbo.Controladora", "Gateway");
            DropColumn("dbo.Controladora", "Mascara");
        }
    }
}
