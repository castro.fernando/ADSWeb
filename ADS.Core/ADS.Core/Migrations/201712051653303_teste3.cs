namespace ADS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class teste3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Advogado", "ExpedidoEm", c => c.DateTime());
            AlterColumn("dbo.Advogado", "RemovidoEm", c => c.DateTime());
            AlterColumn("dbo.Advogado", "AtualizadoEm", c => c.DateTime());
            AlterColumn("dbo.Advogado", "CriadoEm", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Advogado", "CriadoEm", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Advogado", "AtualizadoEm", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Advogado", "RemovidoEm", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Advogado", "ExpedidoEm", c => c.DateTime(nullable: false));
        }
    }
}
