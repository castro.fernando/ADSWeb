namespace ADS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modificadoestruturadeveiculo : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Veiculo", "Modelo_ModeloId", "dbo.Modelo");
            DropIndex("dbo.Veiculo", new[] { "Modelo_ModeloId" });
            RenameColumn(table: "dbo.Veiculo", name: "Modelo_ModeloId", newName: "ModeloId");
            AlterColumn("dbo.Veiculo", "ModeloId", c => c.Int(nullable: false));
            CreateIndex("dbo.Veiculo", "ModeloId");
            AddForeignKey("dbo.Veiculo", "ModeloId", "dbo.Modelo", "ModeloId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Veiculo", "ModeloId", "dbo.Modelo");
            DropIndex("dbo.Veiculo", new[] { "ModeloId" });
            AlterColumn("dbo.Veiculo", "ModeloId", c => c.Int());
            RenameColumn(table: "dbo.Veiculo", name: "ModeloId", newName: "Modelo_ModeloId");
            CreateIndex("dbo.Veiculo", "Modelo_ModeloId");
            AddForeignKey("dbo.Veiculo", "Modelo_ModeloId", "dbo.Modelo", "ModeloId");
        }
    }
}
