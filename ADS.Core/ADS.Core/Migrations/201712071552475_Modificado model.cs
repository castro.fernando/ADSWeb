namespace ADS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modificadomodel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Advogado", "Removido", c => c.Boolean(nullable: false));
            AddColumn("dbo.Endereco", "Removido", c => c.Boolean(nullable: false));
            AddColumn("dbo.Endereco", "RemovidoEm", c => c.DateTime());
            AddColumn("dbo.Endereco", "AtualizadoEm", c => c.DateTime());
            AddColumn("dbo.Endereco", "CriadoEm", c => c.DateTime());
            AddColumn("dbo.Telefone", "Removido", c => c.Boolean(nullable: false));
            AddColumn("dbo.Telefone", "RemovidoEm", c => c.DateTime());
            AddColumn("dbo.Telefone", "AtualizadoEm", c => c.DateTime());
            AddColumn("dbo.Telefone", "CriadoEm", c => c.DateTime());
            AddColumn("dbo.Veiculo", "Removido", c => c.Boolean(nullable: false));
            AddColumn("dbo.Veiculo", "RemovidoEm", c => c.DateTime());
            AddColumn("dbo.Veiculo", "AtualizadoEm", c => c.DateTime());
            AddColumn("dbo.Veiculo", "CriadoEm", c => c.DateTime());
            AddColumn("dbo.Modelo", "Removido", c => c.Boolean(nullable: false));
            AddColumn("dbo.Modelo", "RemovidoEm", c => c.DateTime());
            AddColumn("dbo.Modelo", "AtualizadoEm", c => c.DateTime());
            AddColumn("dbo.Modelo", "CriadoEm", c => c.DateTime());
            AddColumn("dbo.Fabricante", "Removido", c => c.Boolean(nullable: false));
            AddColumn("dbo.Fabricante", "RemovidoEm", c => c.DateTime());
            AddColumn("dbo.Fabricante", "AtualizadoEm", c => c.DateTime());
            AddColumn("dbo.Fabricante", "CriadoEm", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Fabricante", "CriadoEm");
            DropColumn("dbo.Fabricante", "AtualizadoEm");
            DropColumn("dbo.Fabricante", "RemovidoEm");
            DropColumn("dbo.Fabricante", "Removido");
            DropColumn("dbo.Modelo", "CriadoEm");
            DropColumn("dbo.Modelo", "AtualizadoEm");
            DropColumn("dbo.Modelo", "RemovidoEm");
            DropColumn("dbo.Modelo", "Removido");
            DropColumn("dbo.Veiculo", "CriadoEm");
            DropColumn("dbo.Veiculo", "AtualizadoEm");
            DropColumn("dbo.Veiculo", "RemovidoEm");
            DropColumn("dbo.Veiculo", "Removido");
            DropColumn("dbo.Telefone", "CriadoEm");
            DropColumn("dbo.Telefone", "AtualizadoEm");
            DropColumn("dbo.Telefone", "RemovidoEm");
            DropColumn("dbo.Telefone", "Removido");
            DropColumn("dbo.Endereco", "CriadoEm");
            DropColumn("dbo.Endereco", "AtualizadoEm");
            DropColumn("dbo.Endereco", "RemovidoEm");
            DropColumn("dbo.Endereco", "Removido");
            DropColumn("dbo.Advogado", "Removido");
        }
    }
}
