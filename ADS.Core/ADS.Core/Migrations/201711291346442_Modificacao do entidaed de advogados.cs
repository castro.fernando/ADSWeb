namespace ADS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modificacaodoentidaeddeadvogados : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Advogado",
                c => new
                    {
                        AdvogadoId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        CPF = c.String(),
                        RG = c.String(),
                        NumeroInscricao = c.Long(nullable: false),
                        Seccional = c.String(),
                        Subsecao = c.String(),
                        Foto = c.Binary(),
                        Ativo = c.Boolean(nullable: false),
                        RemovidoEm = c.DateTime(nullable: false),
                        AtualizadoEm = c.DateTime(nullable: false),
                        CriadoEm = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.AdvogadoId);
            
            CreateTable(
                "dbo.Endereco",
                c => new
                    {
                        EnderecoId = c.Int(nullable: false, identity: true),
                        CEP = c.String(),
                        UF = c.String(),
                        Cidade = c.String(),
                        Complemento = c.String(),
                        Logradouro = c.String(),
                        Numero = c.Int(nullable: false),
                        PessoaId = c.Int(nullable: false),
                        Pessoa_AdvogadoId = c.Int(),
                    })
                .PrimaryKey(t => t.EnderecoId)
                .ForeignKey("dbo.Advogado", t => t.Pessoa_AdvogadoId)
                .Index(t => t.Pessoa_AdvogadoId);
            
            CreateTable(
                "dbo.Telefone",
                c => new
                    {
                        TelefoneId = c.Int(nullable: false, identity: true),
                        DDD = c.Int(nullable: false),
                        Numero = c.String(),
                        PessoaId = c.Int(nullable: false),
                        Pessoa_AdvogadoId = c.Int(),
                    })
                .PrimaryKey(t => t.TelefoneId)
                .ForeignKey("dbo.Advogado", t => t.Pessoa_AdvogadoId)
                .Index(t => t.Pessoa_AdvogadoId);
            
            CreateTable(
                "dbo.Veiculo",
                c => new
                    {
                        VeiculoId = c.Int(nullable: false, identity: true),
                        PessoaId = c.Int(nullable: false),
                        Modelo_ModeloId = c.Int(),
                        Pessoa_AdvogadoId = c.Int(),
                    })
                .PrimaryKey(t => t.VeiculoId)
                .ForeignKey("dbo.Modelo", t => t.Modelo_ModeloId)
                .ForeignKey("dbo.Advogado", t => t.Pessoa_AdvogadoId)
                .Index(t => t.Modelo_ModeloId)
                .Index(t => t.Pessoa_AdvogadoId);
            
            CreateTable(
                "dbo.Modelo",
                c => new
                    {
                        ModeloId = c.Int(nullable: false, identity: true),
                        FabricanteId = c.Int(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.ModeloId)
                .ForeignKey("dbo.Fabricante", t => t.FabricanteId, cascadeDelete: true)
                .Index(t => t.FabricanteId);
            
            CreateTable(
                "dbo.Fabricante",
                c => new
                    {
                        FabricanteId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.FabricanteId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Veiculo", "Pessoa_AdvogadoId", "dbo.Advogado");
            DropForeignKey("dbo.Veiculo", "Modelo_ModeloId", "dbo.Modelo");
            DropForeignKey("dbo.Modelo", "FabricanteId", "dbo.Fabricante");
            DropForeignKey("dbo.Telefone", "Pessoa_AdvogadoId", "dbo.Advogado");
            DropForeignKey("dbo.Endereco", "Pessoa_AdvogadoId", "dbo.Advogado");
            DropIndex("dbo.Modelo", new[] { "FabricanteId" });
            DropIndex("dbo.Veiculo", new[] { "Pessoa_AdvogadoId" });
            DropIndex("dbo.Veiculo", new[] { "Modelo_ModeloId" });
            DropIndex("dbo.Telefone", new[] { "Pessoa_AdvogadoId" });
            DropIndex("dbo.Endereco", new[] { "Pessoa_AdvogadoId" });
            DropTable("dbo.Fabricante");
            DropTable("dbo.Modelo");
            DropTable("dbo.Veiculo");
            DropTable("dbo.Telefone");
            DropTable("dbo.Endereco");
            DropTable("dbo.Advogado");
        }
    }
}
