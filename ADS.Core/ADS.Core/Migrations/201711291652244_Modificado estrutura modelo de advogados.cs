namespace ADS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modificadoestruturamodelodeadvogados : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Advogado", "ExpedidoEm", c => c.DateTime(nullable: false));
            AddColumn("dbo.Advogado", "Filiacao", c => c.String());
            AlterColumn("dbo.Advogado", "Nome", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Advogado", "Nome", c => c.String());
            DropColumn("dbo.Advogado", "Filiacao");
            DropColumn("dbo.Advogado", "ExpedidoEm");
        }
    }
}
