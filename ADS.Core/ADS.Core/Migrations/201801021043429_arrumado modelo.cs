namespace ADS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class arrumadomodelo : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Endereco", "Pessoa_AdvogadoId", "dbo.Advogado");
            DropForeignKey("dbo.Telefone", "Pessoa_AdvogadoId", "dbo.Advogado");
            DropIndex("dbo.Endereco", new[] { "Pessoa_AdvogadoId" });
            DropIndex("dbo.Telefone", new[] { "Pessoa_AdvogadoId" });
            RenameColumn(table: "dbo.Endereco", name: "Pessoa_AdvogadoId", newName: "AdvogadoId");
            RenameColumn(table: "dbo.Telefone", name: "Pessoa_AdvogadoId", newName: "AdvogadoId");
            AlterColumn("dbo.Endereco", "AdvogadoId", c => c.Int(nullable: false));
            AlterColumn("dbo.Telefone", "AdvogadoId", c => c.Int(nullable: false));
            CreateIndex("dbo.Endereco", "AdvogadoId");
            CreateIndex("dbo.Telefone", "AdvogadoId");
            AddForeignKey("dbo.Endereco", "AdvogadoId", "dbo.Advogado", "AdvogadoId", cascadeDelete: true);
            AddForeignKey("dbo.Telefone", "AdvogadoId", "dbo.Advogado", "AdvogadoId", cascadeDelete: true);
            DropColumn("dbo.Endereco", "PessoaId");
            DropColumn("dbo.Telefone", "PessoaId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Telefone", "PessoaId", c => c.Int(nullable: false));
            AddColumn("dbo.Endereco", "PessoaId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Telefone", "AdvogadoId", "dbo.Advogado");
            DropForeignKey("dbo.Endereco", "AdvogadoId", "dbo.Advogado");
            DropIndex("dbo.Telefone", new[] { "AdvogadoId" });
            DropIndex("dbo.Endereco", new[] { "AdvogadoId" });
            AlterColumn("dbo.Telefone", "AdvogadoId", c => c.Int());
            AlterColumn("dbo.Endereco", "AdvogadoId", c => c.Int());
            RenameColumn(table: "dbo.Telefone", name: "AdvogadoId", newName: "Pessoa_AdvogadoId");
            RenameColumn(table: "dbo.Endereco", name: "AdvogadoId", newName: "Pessoa_AdvogadoId");
            CreateIndex("dbo.Telefone", "Pessoa_AdvogadoId");
            CreateIndex("dbo.Endereco", "Pessoa_AdvogadoId");
            AddForeignKey("dbo.Telefone", "Pessoa_AdvogadoId", "dbo.Advogado", "AdvogadoId");
            AddForeignKey("dbo.Endereco", "Pessoa_AdvogadoId", "dbo.Advogado", "AdvogadoId");
        }
    }
}
