namespace ADS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Atualizacaodaentidadeendereco : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Endereco", "Bairro", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Endereco", "Bairro");
        }
    }
}
