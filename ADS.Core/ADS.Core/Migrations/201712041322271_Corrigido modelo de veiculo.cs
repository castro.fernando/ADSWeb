namespace ADS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Corrigidomodelodeveiculo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Veiculo", "Placa", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Veiculo", "Placa");
        }
    }
}
