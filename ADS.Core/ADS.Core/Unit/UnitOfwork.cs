﻿using ADS.Core.Repositorio;
using ADS.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ADS.Core.Unit
{
    public class UnitOfwork : IDisposable
    {
        private ADSContext _context = new ADSContext();
        private IModeloRepository _modeloRepository;
        private IFabricanteRepository _fabricanteRepository;
        private IAdvogadoRepository _advogadoRepository;
        private IEnderecoRepository _enderecoRepository;
        private ITelefoneRepository _telefoneRepository;
        private IVeiculoRepository _veiculoRepository;
        private IControladoraRepository _controladoraRepository;

        public IControladoraRepository ControladoraRepository
        {
            get
            {
                if(_controladoraRepository == null)
                {
                    _controladoraRepository = new ControladoraRepository(_context);
                }
                return _controladoraRepository;
            }
        }

        public IModeloRepository ModeloRepository
        {
            get
            {
                if (_modeloRepository == null)
                {
                    _modeloRepository = new ModeloRepository(_context);
                }
                return _modeloRepository;
            }
        }

        public IFabricanteRepository FabricanteRepository
        {
            get
            {
                if(_fabricanteRepository == null)
                {
                    _fabricanteRepository = new FabricanteRepository(_context);
                }
                return _fabricanteRepository;
            }
        }

        public IAdvogadoRepository AdvogadoRepository
        {
            get
            {
                if (_advogadoRepository == null)
                {
                    _advogadoRepository = new AdvogadoRepository(_context);
                }
                return _advogadoRepository;
            }
        }

        public IEnderecoRepository EnderecoRepository
        {
            get
            {
                if (_enderecoRepository == null)
                {
                    _enderecoRepository = new EnderecoRepository(_context);
                }
                return _enderecoRepository;
            }
        }

        public ITelefoneRepository TelefoneRepository
        {
            get
            {
                if (_telefoneRepository == null)
                {
                    _telefoneRepository = new TelefoneRepository(_context);
                }
                return _telefoneRepository;
            }
        }

        public IVeiculoRepository VeiculoRepository
        {
            get
            {
                if (_veiculoRepository == null)
                {
                    _veiculoRepository = new VeiculoRepository(_context);
                }
                return _veiculoRepository;
            }
        }

        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
            }
            GC.SuppressFinalize(this);
        }

        public void Salvar()
        {
            _context.SaveChanges();
        }
    }
}