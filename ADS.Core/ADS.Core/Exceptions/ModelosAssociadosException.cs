﻿using System;
using System.Runtime.Serialization;

namespace ADS.Core.Exceptions
{
    [Serializable]
    internal class ModelosAssociadosException : Exception
    {
        public ModelosAssociadosException()
        {
        }

        public ModelosAssociadosException(string message) : base(message)
        {
        }

        public ModelosAssociadosException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ModelosAssociadosException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}