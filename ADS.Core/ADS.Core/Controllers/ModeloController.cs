﻿using ADS.Core.Exceptions;
using ADS.Core.Model;
using ADS.Core.Unit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ADS.Core.Controllers
{
    public class ModeloController : Controller
    {
        private UnitOfwork _unit = new UnitOfwork();

        [HttpGet]
        public ActionResult Cadastrar()
        {
            CarregarListaFabricantes();
            return View();
        }

        private void CarregarListaFabricantes()
        {
            ViewBag.fabricantes = new SelectList(_unit.FabricanteRepository.Listar(), "FabricanteId", "Nome");
        }

        [HttpPost]
        public ActionResult Cadastrar(Modelo modelo)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unit.ModeloRepository.Cadastrar(modelo);
                    _unit.Salvar();
                    TempData["msg"] = "Modelo cadastrado com sucesso!";
                    return RedirectToAction("Cadastrar");
                }
                else
                {
                    CarregarListaFabricantes();
                    return View(modelo);
                }
                
            }catch(Exception e)
            {
                TempData["msg"] = "Falha ao cadastrar modelo!" + e.Message;
                return View(modelo);
            }
        }

        [HttpGet]
        public ActionResult Alterar(int id)
        {
            CarregarListaFabricantes();
            return View(_unit.ModeloRepository.Buscar(id));
        }

        [HttpPost]
        public ActionResult Alterar(Modelo modelo)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unit.ModeloRepository.Alterar(modelo);
                    _unit.Salvar();
                    TempData["msg"] = "Modelo alterado com sucesso!";
                    return RedirectToAction("Listar");
                }
                else
                {
                    return View(modelo);
                }
            }catch(Exception e)
            {
                TempData["msg"] = "Falha ao alterar modelo! " +e.Message;
                return View(modelo);
            }
        }

        public ActionResult Remover(int id)
        {
            try
            {
                _unit.ModeloRepository.Remover(id);
                _unit.Salvar();
            }
            catch (ModelosAssociadosException e)
            {
                TempData["msg"] = "Falha ao remover modelo! " + e.Message;
            }        
            return RedirectToAction("Listar");
        }

        [HttpGet]
        public ActionResult Listar(int? pagina, int? qtdPorPagina)
        {
            Pager pager = new Pager(_unit.ModeloRepository.QuantidadeModelo(), pagina, qtdPorPagina != null ? (int)qtdPorPagina : 10);
            ViewBag.Pager = pager;
            return View(_unit.ModeloRepository.ListarPaginado(pager));
        }

        protected override void Dispose(bool disposing)
        {
            _unit.Dispose();
            base.Dispose(disposing);
        }

    }
}