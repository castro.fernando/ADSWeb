﻿using ADS.Core.Model;
using ADS.Core.Unit;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ADS.Core.Controllers
{
    public class AdvogadoController : Controller
    {
        private UnitOfwork _unit = new UnitOfwork(); 
        private Advogado adv = new Advogado();
        private IList<Endereco> enderecos;
        private IList<Telefone> telefones;
        private IList<Veiculo> veiculos;
        private Dictionary<int, bool> idsSelecionados = new Dictionary<int, bool>();

        public ActionResult Importar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Importar(HttpPostedFileBase uploadFile)
        {
            int cadastrados = 0;
            int atualizados = 0;
            try
            {
                if (uploadFile.ContentLength > 0)
                {
                    string filePath = Path.Combine(HttpContext.Server.MapPath("../Uploads"),
                   Path.GetFileName(uploadFile.FileName));
                    uploadFile.SaveAs(filePath);
                    DataSet ds = new DataSet();

                    //A 32-bit provider which enables the use of
                    string connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=Yes;IMEX=1\";", filePath);

                    using (OleDbConnection conn = new System.Data.OleDb.OleDbConnection(connectionString))
                    {
                        conn.Open();
                        using (DataTable dtExcelSchema = conn.GetSchema("Tables"))
                        {
                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                            string query = "SELECT * FROM [" + sheetName + "]";
                            OleDbDataAdapter adapter = new OleDbDataAdapter(query, conn);
                            //DataSet ds = new DataSet();
                            adapter.Fill(ds, "Items");
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                                    {
                                        if (string.IsNullOrEmpty(ds.Tables[0].Rows[i]["NumeroCarteira"].ToString()))
                                        {
                                            break;
                                        }
                                        Advogado adv = new Advogado()
                                        {
                                            Nome = ds.Tables[0].Rows[i]["Nome"].ToString(),
                                            RG = ds.Tables[0].Rows[i]["RG"].ToString(),
                                            CPF = ds.Tables[0].Rows[i]["CPF"].ToString(),
                                            NumeroCarteira = Convert.ToInt64(ds.Tables[0].Rows[i]["NumeroCarteira"].ToString().Trim()),
                                            NumeroInscricao = Convert.ToInt64(ds.Tables[0].Rows[i]["NumeroInscricao"].ToString().Trim()),
                                            Seccional = ds.Tables[0].Rows[i]["Seccional"].ToString(),
                                            Subsecao = ds.Tables[0].Rows[i]["Subseccao"].ToString(),
                                            Ativo = ds.Tables[0].Rows[i]["Ativo"].ToString().Trim().ToUpper() == "SIM" ? true : false,
                                            Veiculo = new List<Veiculo>(),
                                            Endereco = new List<Endereco>(),
                                            Telefone = new List<Telefone>(),
                                            CriadoEm = Convert.ToDateTime(System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"))
                                        };
                                        var result = _unit.AdvogadoRepository.BuscarPor(a => a.NumeroInscricao == adv.NumeroInscricao && a.Removido == false);

                                        if (result.Count > 0)
                                        {
                                            result.FirstOrDefault().Nome = adv.Nome;
                                            result.FirstOrDefault().RG = adv.RG;
                                            result.FirstOrDefault().CPF = adv.CPF;
                                            result.FirstOrDefault().NumeroCarteira = adv.NumeroCarteira;
                                            result.FirstOrDefault().NumeroInscricao = adv.NumeroInscricao;
                                            result.FirstOrDefault().Seccional = adv.Seccional;
                                            result.FirstOrDefault().Subsecao = adv.Subsecao;
                                            result.FirstOrDefault().Ativo = adv.Ativo;
                                            result.FirstOrDefault().AtualizadoEm = Convert.ToDateTime(System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                                            foreach (var v in result.FirstOrDefault().Veiculo)
                                            {
                                                v.Modelo = _unit.ModeloRepository.Buscar(v.ModeloId);
                                            }
                                            atualizados++;
                                            _unit.AdvogadoRepository.Alterar(result.FirstOrDefault());
                                        }
                                        else
                                        {
                                            cadastrados++;
                                            _unit.AdvogadoRepository.Cadastrar(adv);
                                        }
                                        _unit.Salvar();
                                        //Now we can insert this data to database...
                                    }
                                }
                            }
                        }
                    }
                }
                TempData["msg"] = "Inseridos: " + cadastrados + " Atualizados: " + atualizados;
                return RedirectToAction("Listar");
            }catch(Exception e)
            {
                TempData["msg"] = "Erro ao importar lista. " + e.Message;
                return RedirectToAction("Importar");
            }
            
        }

        [HttpGet]
        public ActionResult Cadastrar()
        {
            adv.Endereco = new List<Endereco>();
            adv.Veiculo = new List<Veiculo>();
            adv.Telefone = new List<Telefone>();
            CarregarListaFabricantes();
            ViewBag.modelos = new SelectList(new List<Modelo>());
            enderecos = new List<Endereco>();
            telefones = new List<Telefone>();
            veiculos = new List<Veiculo>();
            Session["enderecos"] = enderecos;
            Session["telefones"] = telefones;
            Session["veiculos"] = veiculos;
            return View(adv);
        }

        [HttpGet]
        public ActionResult Editar(int id)
        {
            CarregarListaFabricantes();
            ViewBag.modelos = new SelectList(new List<Modelo>());
            enderecos = new List<Endereco>();
            telefones = new List<Telefone>();
            veiculos = new List<Veiculo>();
            Session["enderecos"] = enderecos;
            Session["telefones"] = telefones;
            Session["veiculos"] = veiculos;
            var adv = _unit.AdvogadoRepository.Buscar(id);
            Session["enderecos"] = adv.Endereco.Where(e => e.Removido == false).ToList();
            Session["telefones"] = adv.Telefone;
            adv.Veiculo = (IList<Veiculo>)_unit.VeiculoRepository.BuscarPor(v => v.Advogado.AdvogadoId == adv.AdvogadoId);
            Session["veiculos"] = adv.Veiculo;
            return View(adv);
        }

        [HttpPost]
        public ActionResult Editar(Advogado advogado)
        {
            advogado.Endereco = (List<Endereco>)Session["enderecos"];
            advogado.Telefone = (List<Telefone>)Session["telefones"];
            advogado.Veiculo = (List<Veiculo>)Session["veiculos"];
            advogado.AtualizadoEm = Convert.ToDateTime(System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            advogado.RemovidoEm = null;
            advogado.ExpedidoEm = null;
            foreach (var e in advogado.Endereco)
            {
                if (e.EnderecoId == 0)
                {
                    e.CriadoEm = Convert.ToDateTime(System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                    e.AdvogadoId = advogado.AdvogadoId;
                    _unit.EnderecoRepository.Cadastrar(e);                    
                }
                else
                {
                    e.AtualizadoEm = Convert.ToDateTime(System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                    e.AdvogadoId = advogado.AdvogadoId;
                    e.Advogado = null;
                    _unit.EnderecoRepository.Alterar(e);
                }          
            }
            foreach (var e in advogado.Telefone)
            {
                if(e.TelefoneId == 0)
                {
                    e.CriadoEm = Convert.ToDateTime(System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                    e.AdvogadoId = advogado.AdvogadoId;
                    _unit.TelefoneRepository.Cadastrar(e);
                }
                else
                {
                    e.AtualizadoEm = Convert.ToDateTime(System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                    e.AdvogadoId = advogado.AdvogadoId;
                    e.Advogado = null;
                    _unit.TelefoneRepository.Alterar(e);
                }           
            }
            foreach (var e in advogado.Veiculo.Where(v=>v.Removido == false))
            {
                if (e.VeiculoId == 0)
                {
                    e.CriadoEm = Convert.ToDateTime(System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                    e.AdvogadoId = advogado.AdvogadoId;
                    _unit.VeiculoRepository.Cadastrar(e);
                }
                else
                {
                    e.AtualizadoEm = Convert.ToDateTime(System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                    if(e.Removido == true)
                    {
                        e.RemovidoEm = Convert.ToDateTime(System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                    }
                    e.AdvogadoId = advogado.AdvogadoId;
                    e.Advogado = null;
                    _unit.VeiculoRepository.Alterar(e);
                }
            }

                if (ModelState.IsValid)
            {
                
                _unit.AdvogadoRepository.Alterar(advogado);               
                _unit.Salvar();
                TempData["msg"] = "Advogado atualizado com sucesso";
                return RedirectToAction("Listar");
            }
            else
            {
                CarregarListaFabricantes();
                ViewBag.modelos = new SelectList(new List<Modelo>());
                TempData["msg"] = "Falha ao atualizar advogado.";
                return View(advogado);
            }

        }


        [HttpPost]
        public ActionResult Cadastrar(Advogado advogado)
        {            
            advogado.Endereco = (List<Endereco>)Session["enderecos"];
            advogado.Telefone = (List<Telefone>)Session["telefones"];
            advogado.Veiculo = (List<Veiculo>)Session["veiculos"];
            if (_unit.AdvogadoRepository.BuscarPor(a => a.NumeroCarteira == advogado.NumeroCarteira || a.NumeroInscricao == advogado.NumeroInscricao).Count > 0)
            {
                CarregarListaFabricantes();
                ViewBag.modelos = new SelectList(new List<Modelo>());
                TempData["msg"] = "Existe outro registro com mesmo número de carteira / inscrição! O número deve ser único.";
                return View(advogado);
            }
            advogado.CriadoEm = Convert.ToDateTime(System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            foreach(var e in advogado.Endereco)
            {
                e.CriadoEm = advogado.CriadoEm;
            }
            foreach(var t in advogado.Telefone)
            {
                t.CriadoEm = advogado.CriadoEm;
            }
            foreach(var v in advogado.Veiculo)
            {
                v.CriadoEm = advogado.CriadoEm;
            }
            advogado.AtualizadoEm = null;
            advogado.RemovidoEm = null;
            advogado.ExpedidoEm = null;
            if (ModelState.IsValid)
            {
                _unit.AdvogadoRepository.Cadastrar(advogado);
                _unit.Salvar();
                TempData["msg"] = "Advogado cadastrado com sucesso";
                return RedirectToAction("Cadastrar");
            }
            else
            {
                CarregarListaFabricantes();
                ViewBag.modelos = new SelectList(new List<Modelo>());
                TempData["msg"] = "Falha ao adicionar advogado.";
                return View(advogado);
            }
            
        }

        [HttpPost]
        public ActionResult Remover(int id)
        {
            var adv = _unit.AdvogadoRepository.Buscar(id);
            adv.Removido = true;
            adv.RemovidoEm = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyy HH:mm:ss"));
            foreach(var e in adv.Endereco)
            {
                e.Removido = true;
                e.RemovidoEm = adv.RemovidoEm;
                _unit.EnderecoRepository.Alterar(e);
            }
            foreach(var t in adv.Telefone)
            {
                t.Removido = true;
                t.RemovidoEm = adv.RemovidoEm;               
            }
            foreach (var t in adv.Veiculo)
            {
                t.Modelo = _unit.ModeloRepository.Buscar(t.ModeloId);
                t.Removido = true;
                t.RemovidoEm = adv.RemovidoEm;
            }
            _unit.AdvogadoRepository.Alterar(adv);
            _unit.Salvar();
            TempData["msg"] = "Advogado removido com sucesso.";
            return RedirectToAction("Listar");
        }

        [HttpPost]
        public void gravarCheckboxSelecionado(int id, bool selecionado)
        {
            if(Session["idsSelecionados"] == null)
            {
                Session["idsSelecionados"] = new Dictionary<int, bool>();
            }
            else
            {
                idsSelecionados = (Dictionary<int, bool>)Session["idsSelecionados"];
            }
            if (selecionado == true)
            {
                idsSelecionados.Add(id, selecionado);
            }
            else
            {
                idsSelecionados.Remove(id);
            }
            idsSelecionados.FirstOrDefault(a => a.Key == 1);
            Session["idsSelecionados"] = idsSelecionados;
        }

        public ActionResult Listar(int? pagina, int? qtdPorPagina)
        {
            Pager pager = new Pager(_unit.AdvogadoRepository.QuantidadeDeAdvogados(), pagina, qtdPorPagina != null ? (int)qtdPorPagina : 10);
            ViewBag.Pager = pager;
            ViewBag.idsSelecionados = idsSelecionados;
            return View(_unit.AdvogadoRepository.ListarPaginado(pager));
        }

        [HttpPost]
        public ActionResult GetModelosJson(int id)
        {
            if (id != 0)
            {
                //Values are hard coded for demo. you may replae with values 
                // coming from your db/service based on the passed in value ( val.Value)

                return Json(new { Success = "true", Modelos = _unit.ModeloRepository.ListarPorFabricante(id) });
            }
            return Json(new { Success = "false" });
        }

        [HttpPost]
        public ActionResult AdicionarEndereco(Endereco endereco)
        {
            if (Session["enderecos"] != null)
            {
                enderecos = (IList<Endereco>)Session["enderecos"];
            }
            if (endereco.EnderecoId == 0)
            {
                enderecos.Add(endereco);
            }
            else
            {
                enderecos.RemoveAt(endereco.EnderecoId-1);
                enderecos.Insert(endereco.EnderecoId-1, endereco);
            }
            
            Session.Add("enderecos", enderecos);
            
            return PartialView("_ListaEndereco", enderecos);
        }

        [HttpPost]
        public ActionResult AdicionarEndereco1(Endereco endereco)
        {
            if (Session["enderecos"] != null)
            {
                enderecos = (IList<Endereco>)Session["enderecos"];
            }
            if (endereco.EnderecoId == 0)
            {
                enderecos.Add(endereco);
            }
            else
            {                
                enderecos.Remove(enderecos.Where(e=>e.EnderecoId == endereco.EnderecoId).FirstOrDefault());
                enderecos.Add(endereco);
            }

            Session.Add("enderecos", enderecos);

            return PartialView("_ListaEndereco", enderecos.Where(e=>e.Removido == false));
        }

        [HttpGet]
        public ActionResult BuscarEndereco(int id)
        {
            enderecos = (IList<Endereco>)Session["enderecos"];
            return Json(new {Endereco = enderecos.ElementAt(id-1)} , JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult BuscarEndereco1(int id)
        {
            enderecos = (IList<Endereco>)Session["enderecos"];
            return Json(new { Endereco = enderecos.Where(e=>e.EnderecoId == id) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public PartialViewResult DetalhesEndereco(int id)
        {
            enderecos = (IList<Endereco>)Session["enderecos"];
            return PartialView("_DetalhesEndereco", enderecos.ElementAt(id-1));
        }

        [HttpGet]
        public PartialViewResult DetalhesEndereco1(int id)
        {
            enderecos = (IList<Endereco>)Session["enderecos"];
            return PartialView("_DetalhesEndereco", enderecos.Where(e=>e.EnderecoId == id).FirstOrDefault());
        }

        [HttpPost]
        public PartialViewResult RemoverEndereco(int id)
        {
            enderecos = (IList<Endereco>)Session["enderecos"];
            enderecos.RemoveAt(id - 1);
            Session.Add("enderecos", enderecos);
            return PartialView("_ListaEndereco", enderecos);
        }

        [HttpPost]
        public PartialViewResult RemoverEndereco1(int id)
        {
            enderecos = (IList<Endereco>)Session["enderecos"];
            var end = enderecos.Where(e => e.EnderecoId == id).FirstOrDefault();
            end.Removido = true;
            end.RemovidoEm = Convert.ToDateTime(System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            enderecos.Remove(enderecos.Where(e => e.EnderecoId == id).FirstOrDefault());
            enderecos.Add(end);
            return PartialView("_ListaEndereco", enderecos);
        }



        public PartialViewResult ListarEnderecos(Advogado adv)
        {           
            return PartialView("_ListaEndereco", new Endereco());
        }

        private void CarregarListaFabricantes()
        {
            ViewBag.fabricantes = new SelectList(_unit.FabricanteRepository.Listar(), "FabricanteId", "Nome");
        }

        private void CarregarListaModelos()
        {
            ViewBag.modelos = new SelectList(_unit.ModeloRepository.Listar(),  "ModeloId", "Nome");
        }


        public PartialViewResult AdicionarTelefone(Telefone telefone)
        {
            if (Session["telefones"] != null)
            {
                telefones = (IList<Telefone>)Session["telefones"];
            }
            if (telefone.TelefoneId == 0)
            {
                telefones.Add(telefone);
            }
            else
            {
                telefones.RemoveAt(telefone.TelefoneId - 1);
                telefones.Insert(telefone.TelefoneId - 1, telefone);
            }

            Session.Add("telefones", telefones);

            return PartialView("_ListaTelefone", telefones);
        }

        public PartialViewResult AdicionarTelefone1(Telefone telefone)
        {
            if (Session["telefones"] != null)
            {
                telefones = (IList<Telefone>)Session["telefones"];
            }
            if (telefone.TelefoneId == 0)
            {
                telefones.Add(telefone);
            }
            else
            {
                telefone.CriadoEm = telefones.Where(t => t.TelefoneId == telefone.TelefoneId).FirstOrDefault().CriadoEm;
                telefones.Remove(telefones.Where(t=>t.TelefoneId == telefone.TelefoneId).FirstOrDefault());
                telefones.Add(telefone);
            }
            Session.Add("telefones", telefones);
            return PartialView("_ListaTelefone", telefones);
        }

        [HttpGet]
        public ActionResult BuscarTelefone(int id)
        {
            telefones = (IList<Telefone>)Session["telefones"];
            return Json(new { Telefone = telefones.ElementAt(id - 1) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult BuscarTelefone1(int id)
        {
            telefones = (IList<Telefone>)Session["telefones"];
            return Json(new { Telefone = telefones.Where(t=>t.TelefoneId==id) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public PartialViewResult RemoverTelefone(int id)
        {
            telefones = (IList<Telefone>)Session["telefones"];
            telefones.RemoveAt(id - 1);
            Session.Add("telefones", telefones);
            return PartialView("_ListaTelefone", telefones);
        }

        [HttpPost]
        public PartialViewResult RemoverTelefone1(int id)
        {
            telefones = (IList<Telefone>)Session["telefones"];
            var tel = telefones.Where(e => e.TelefoneId == id).FirstOrDefault();
            tel.Removido = true;
            tel.RemovidoEm = Convert.ToDateTime(System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            telefones.Remove(telefones.Where(e => e.TelefoneId == id).FirstOrDefault());
            telefones.Add(tel);
            Session.Add("telefones", telefones);
            return PartialView("_ListaTelefone", telefones);
        }

        public PartialViewResult AdicionarVeiculo(Veiculo veiculo)
        {
            var modelo = _unit.ModeloRepository.Buscar(veiculo.ModeloId);
            veiculo.Modelo = modelo;
            if (Session["veiculos"] != null)
            {
                veiculos = (IList<Veiculo>)Session["veiculos"];
            }
            if (veiculo.VeiculoId == 0)
            {
                veiculos.Add(veiculo);
            }
            else
            {
                veiculos.RemoveAt(veiculo.VeiculoId -1);
                veiculos.Insert(veiculo.VeiculoId -1, veiculo);
            }

            Session.Add("veiculos", veiculos);

            return PartialView("_ListaVeiculo", veiculos);
        }

        public PartialViewResult AdicionarVeiculo1(Veiculo veiculo)
        {
            var modelo = _unit.ModeloRepository.Buscar(veiculo.ModeloId);
            veiculo.Modelo = modelo;
            if (Session["veiculos"] != null)
            {
                veiculos = (IList<Veiculo>)Session["veiculos"];
            }
            if (veiculo.VeiculoId == 0)
            {
                veiculo.CriadoEm=Convert.ToDateTime(System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                veiculos.Add(veiculo);
            }
            else
            {
                var vei = _unit.VeiculoRepository.Buscar(veiculo.VeiculoId);
                veiculo.CriadoEm = vei.CriadoEm;
                veiculo.AtualizadoEm = Convert.ToDateTime(System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                veiculos.Remove(veiculos.Where(v => v.VeiculoId == veiculo.VeiculoId).FirstOrDefault());
                veiculos.Add(veiculo);
            }

            Session.Add("veiculos", veiculos);

            return PartialView("_ListaVeiculo", veiculos);
        }

        [HttpGet]
        public ActionResult BuscarVeiculo(int id)
        {
            veiculos = (IList<Veiculo>)Session["veiculos"];
            Veiculo v = veiculos.ElementAt(id - 1);
            //v.Modelo = _unit.ModeloRepository.Buscar(v.Modelo.ModeloId);
            return Json(new { Veiculo = v }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult BuscarVeiculo1(int id)
        {
            veiculos = (IList<Veiculo>)Session["veiculos"];
            Veiculo v = veiculos.Where(c => c.VeiculoId == id).FirstOrDefault();
            //v.Modelo = _unit.ModeloRepository.Buscar(v.Modelo.ModeloId);
            return Json(new { Veiculo = v }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public PartialViewResult RemoverVeiculo(int id)
        {
            veiculos = (IList<Veiculo>)Session["veiculos"];
            veiculos.RemoveAt(id - 1);
            Session.Add("veiculos", veiculos);
            return PartialView("_ListaVeiculo", veiculos);
        }

        [HttpPost]
        public PartialViewResult RemoverVeiculo1(int id)
        {
            veiculos = (IList<Veiculo>)Session["veiculos"];
            var veiculo = veiculos.Where(v => v.VeiculoId == id).FirstOrDefault();
            veiculos.Remove(veiculo);
            veiculo.Removido = true;
            veiculo.RemovidoEm = Convert.ToDateTime(System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            veiculos.Add(veiculo);
            Session.Add("veiculos", veiculos);
            return PartialView("_ListaVeiculo", veiculos);
        }

        [HttpGet]
        public PartialViewResult DetalhesAdvogado(int id)
        {
            return PartialView("_DetalhesAdvogado", _unit.AdvogadoRepository.Buscar(id));
        }

        public PartialViewResult GetVeiculos()
        {
            return PartialView("_ListaVeiculo", (IList<Veiculo>)Session["veiculos"]);
        }
        public PartialViewResult GetEnderecos()
        {
            return PartialView("_ListaEndereco", (IList<Endereco>)Session["enderecos"]);
        }
        public PartialViewResult GetTelefones()
        {
            return PartialView("_ListaTelefone", (IList<Telefone>)Session["telefones"]);
        }

    }
  
}