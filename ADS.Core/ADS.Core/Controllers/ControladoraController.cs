﻿using ADS.Core.Model;
using ADS.Core.Unit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ADS.Core.Controllers
{
    public class ControladoraController : Controller
    {
        private UnitOfwork _unit = new UnitOfwork();

        [HttpGet]
        public ActionResult Cadastrar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Cadastrar(Controladora controladora)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    controladora.CriadoEm = Convert.ToDateTime(System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                    _unit.ControladoraRepository.Cadastrar(controladora);
                    _unit.Salvar();
                    TempData["msg"] = "Controladora salva com sucesso!";
                    return RedirectToAction("Cadastrar");
                }
            }
            catch(Exception e)
            {
                TempData["msg"] = "Falha ao cadastrar controladora. " + e.Message;   
            }
            return View(controladora);
        }

        public ActionResult Listar(int? pagina, int? qtdPorPagina)
        {
            Pager pager = new Pager(_unit.ControladoraRepository.QuantidadeControladora(),pagina, qtdPorPagina == null ? 10 : (int)qtdPorPagina);
            ViewBag.Pager = pager;
            return View(_unit.ControladoraRepository.ListarPaginado(pager));
        }

        public PartialViewResult Detalhes(int id)
        {
            return PartialView("_Detalhes",_unit.ControladoraRepository.Buscar(id));
        }

        [HttpPost]
        public ActionResult Remover(int id)
        {
            try
            {
                _unit.ControladoraRepository.Remover(id);
                _unit.Salvar();
                TempData["msg"] = "Controladora removida com sucesso!";
            }catch(Exception e)
            {
                TempData["msg"] = "Falha ao remover controladora. " + e.Message;
            }
            return RedirectToAction("Listar");
        }

        public ActionResult Editar(int id)
        {
            return View(_unit.ControladoraRepository.Buscar(id));
        }

        [HttpPost]
        public ActionResult Editar(Controladora controladora)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    controladora.AtualizadoEm = Convert.ToDateTime(System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                    _unit.ControladoraRepository.Alterar(controladora);
                    _unit.Salvar();
                    TempData["msg"] = "Controladora alterada com sucesso!";
                    return RedirectToAction("Listar");
                }
            }
            catch (Exception e)
            {
                TempData["msg"] = "Falha ao alterar controladora. " + e.Message;
            }
            return View(controladora);
        }
    }
}