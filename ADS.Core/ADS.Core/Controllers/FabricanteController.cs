﻿using ADS.Core.Model;
using ADS.Core.Unit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ADS.Core.Controllers
{
    public class FabricanteController : Controller
    {
        private UnitOfwork _unit = new UnitOfwork();

        [HttpGet]
        public ActionResult Cadastrar()
        {
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Cadastrar(Fabricante fabricante)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unit.FabricanteRepository.Cadastrar(fabricante);
                    _unit.Salvar();
                    TempData["msg"] = "Fabricante cadastrado com sucesso";
                    return RedirectToAction("Cadastrar");
                }
            }catch(Exception e)
            {
                TempData["msg"] = "Falha ao cadastrar fabricante. " + e.Message;
            }           
            return View(fabricante);
        }

        [HttpGet]
        public ActionResult Listar(int? pagina, int? qtdPorPagina)
        {
            Pager pager = new Pager(_unit.FabricanteRepository.QuantidadeFabricante(), pagina, qtdPorPagina != null ? (int)qtdPorPagina : 10);
            ViewBag.Pager = pager;
            return View(_unit.FabricanteRepository.ListarPaginado(pager));
        }

        public ActionResult Remover(int id)
        {
            try
            {
                _unit.FabricanteRepository.Remover(id);
                _unit.Salvar();
                TempData["msg"] = "Fabricante removido com sucesso!";
            }catch(Exception e)
            {
                TempData["msg"] = "Falha ao remover fabricante! " +e.Message;
            }
            return RedirectToAction("Listar");
        }

        [HttpPost]
        public ActionResult Alterar(Fabricante fabricante)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    _unit.FabricanteRepository.Alterar(fabricante);
                    _unit.Salvar();
                    TempData["msg"] = "Fabricante alterado com sucesso";
                    return RedirectToAction("Listar");
                }
                else
                {
                    return View(fabricante);
                }
            }
            catch(Exception e)
            {
                TempData["msg"] = "Falha ao alterar fabricante. " + e.Message;
                return View(fabricante);
            }
        }

        public ActionResult Alterar(int id)
        {
            return View(_unit.FabricanteRepository.Buscar(id));
        }

        protected override void Dispose(bool disposing)
        {
            if (_unit != null)
            {
                _unit.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}